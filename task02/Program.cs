﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task02
{
    class Program
    {
        static void Main(string[] args)
        {
            method();
            method();
            method();
            method();
        }
        static void method ()
        {
            Console.WriteLine("This is a method");
            Console.WriteLine("This second line is now printed to the screen");
        }
    }
}
